#spring-demo
[TOC]
> Spring Demo，关于Spring的Demo程序，从里面可以找到很多有用的代码片段，目前是基于Spring boot版本；

##包括以下Module
+ spring-boot -- 最主要的spring boot示例程序；
+ service-provider -- Spring Cloud服务提供者示例；
+ service-consumer -- Spring Cloud服务消费者示例；
+ eureka-server -- Spring Cloud eureka；
+ eureka-client -- Spring Cloud eureka；

---

##文档

###（一）Spring Boot转换为常规WAR包（spring-boot module）
Spring Boot应用默认打成可执行Jar包，里面包括所有依赖的包，使用内嵌的Tomcat或者Jetty容器。如果想要将Spring Boot项目打成普通的WAR包，再部署到Server上，那么需要做如下修改：
######1. 修改`pom.xml`中packaging为war：`<packaging>war</packaging>`；
######2. 依赖项`spring-boot-starter-tomcat`的scope修改为provided；
表示打包时不再需要嵌入的tomcat包：
```xml 
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-tomcat</artifactId>
	<scope>provided</scope>
</dependency>
```
######3. 创建src/main/webapp目录，作为Web应用根目录存放相关文件；
同时创建`src/main/webapp/WEB-INF/web.xml`文件（注意：`servlet3.0`以上版本不需要web.xml文件也是可以的，如果是`Servlet2.5`及以下的版本的容器，则需要通过在web.xml文件中增加加载Spring的代码，如配置Listener和DispatchServlet）；原来从src/main/resources加载的templates等文件移到到webapp下面。默认情况下，Spring boot不需要创建webapp目录，其加载的静态资源文件比如模板文件、CSS文件都是从src/main/resources中加载（通过`classpath`）；修改为WAR包后，需要做相应的调整；
######4. 入口`MyApplication`类需要`extends SpringBootServletInitializer`，直接实现WebApplicationInitializer是为了支持部署到WebLogic：
```java
@SpringBootApplication
@EnableAutoConfiguration
@EnableScheduling
public class MyApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

	public static void main(String[] args) {
		SpringApplication.run(MyApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MyApplication.class);
	}

}
```

######5. 打成WAR包后，照样可以通过`mvn spring-boot:run`直接采用内嵌Tomcat或者Jetty形式运行；
######6. 转换为WAR过后，需要注意各Controller的URL路径，不然很容易出现404；
######7. 出现404的解决方案：
> （1）thymeleaf模板文件，目录由`src/main/resources/templates ---> src/main/webapp/templates`，需要在application.properties中配置：`spring.thymeleaf.prefix=/templates/`；
> （2）引用的JS、CSS等，目录：`src/main/resources/static ---> src/main/webapp/static`，在模板文件中引用时，需要添加static，例如：由`@{/bootstrap/css/bootstrap.min.css}`变为`@{/static/bootstrap/css/bootstrap.min.css}`；

####参考材料
* [SpringBoot Docs -- build-tool-plugins-maven-packaging](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#build-tool-plugins-maven-packaging)
* [SpringBoot Docs -- howto-create-a-deployable-war-file](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-create-a-deployable-war-file)

---
##待办事宜##
- [X] Spring boot打成WAR包；
- [X] 通过插入HTML注释的方式，使有序编号连续，OS码云不支持此种方式；
- [ ] 待完善更多内容


##参考资料##
[cmd-markdown-高阶语法手册](https://www.zybuluo.com/mdeditor?url=https://www.zybuluo.com/static/editor/md-help.markdown#cmd-markdown-高阶语法手册)