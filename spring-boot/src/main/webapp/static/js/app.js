/**
 * Applicaton entry for layui.
 */

layui.config({
	base : 'modules/', // 你存放新模块的目录，注意，不是layui的模块目录
	version: '1511048241366' //为了更新 js 缓存，可忽略
}); // 加载入口

layui.use([ 'layer', 'form', 'upload', 'table' ], function() { // 如果只加载一个模块，可以不填数组。如：layui.use('form')
	var form = layui.form; // 获取form模块
	var upload = layui.upload; // 获取upload模块
	var layer = layui.layer;
	var table = layui.table;

	// 监听提交按钮
	form.on('submit(test)', function(data) {
		console.log(data);
	});

	// FIXME 自己的代码写到这里
	layer.msg('Hello World');
	console.log("Hello World");
});

