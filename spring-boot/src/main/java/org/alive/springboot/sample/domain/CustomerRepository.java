package org.alive.springboot.sample.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring jdbc repository
 * 
 * @author xuhailin730
 *
 */
@Repository("customerRepo")
public class CustomerRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void createTable() {
		jdbcTemplate.execute("DROP TABLE customer IF EXISTS");
		jdbcTemplate.execute("CREATE TABLE customer("
				+ "id SERIAL primary key, firstname VARCHAR(255), lastname VARCHAR(255), birthDay date, welthy double)");
	}

	@Transactional(readOnly = true)
	public List<Customer> findAll() {
		return jdbcTemplate.query("select * from customer", new CustomerRowMapper());
	}

	public Customer findById(long id) {
		try {
			return jdbcTemplate.queryForObject("select * from customer where id=?", new Object[] { id },
					new CustomerRowMapper());
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	public Customer create(final Customer c) {
		final String sql = "insert into customer(firstname, lastname, birthday, welthy) values (?, ?, ?, ?)";

		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, c.getFirstName());
				ps.setString(2, c.getLastName());
				ps.setDate(3, c.getBirthDay());
				ps.setDouble(4, c.getWelthy());
				return ps;
			}
		}, holder);
		long newId = holder.getKey().longValue();
		c.setId(newId);
		return c;
	}

	public int delete(final long id) {
		final String sql = "delete from customer where id=?";
		return jdbcTemplate.update(sql, new Object[] { id }, new int[] { java.sql.Types.INTEGER });
	}

	public int update(final Customer c) {
		return jdbcTemplate.update("update users set firstname=?, lastname=?, birthday=?, welthy=? where id=?",
				new Object[] { c.getFirstName(), c.getLastName(), c.getBirthDay(), c.getWelthy(), c.getId() });
	}

	static class CustomerRowMapper implements RowMapper<Customer> {

		@Override
		public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
			Customer c = new Customer();
			c.setId(rs.getLong("id"));
			c.setFirstName(rs.getString("firstname"));
			c.setLastName(rs.getString("lastname"));
			c.setBirthDay(rs.getDate("birthday"));
			c.setWelthy(rs.getDouble("welthy"));
			return c;
		}
	}
}
