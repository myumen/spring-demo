package org.alive.springboot.sample.service;

import java.util.List;

import org.alive.springboot.sample.domain.Customer;
import org.alive.springboot.sample.domain.CustomerRepository;
import org.alive.springboot.sample.mapper.CustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepo;
	
	public void createTable() {
		customerRepo.createTable();
	}
	
	public List<Customer> findAll() {
		return customerRepo.findAll();
	}
	
	public Customer findById(long id) {
		return customerRepo.findById(id);
	}
	
	public Customer create(final Customer c) {
		return customerRepo.create(c);
	}
	
	public int delete(final long id) {
		return customerRepo.delete(id);
	}
	
	public int update(final Customer c) {
		return customerRepo.update(c);
	}
	
	// Mybatis
	@Autowired
	private CustomerMapper mapper;
	
	public Customer findByName(String firstname) {
		return mapper.findByName(firstname);
	}
}
