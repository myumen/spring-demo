package org.alive.springboot.sample.controller;

import java.sql.Date;
import java.util.List;

import org.alive.springboot.sample.domain.Customer;
import org.alive.springboot.sample.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * CRUD controller.
 * 
 * <P>
 * 
 * </p>
 * 
 * @author xuhailin730
 *
 */
@Controller
@RequestMapping("customer") // <context-root>/customer
public class CustomerController implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private CustomerService service;

	/**
	 * customer index页面，
	 * RequestMapping不需要添加/，访问的URI为<context-root>/customer和<context-root>/customer/，如果添加了/，则URL中必须带斜线才行
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("")
	public String customer(Model model) {
		List<Customer> custList = service.findAll();
		model.addAttribute("MSG", "");
		model.addAttribute("custList", custList);
		return "customer";
	}

	// @RequestMapping("/{flag}/{id)")
	// @ResponseBody
	// public Customer operate(@PathVariable String flag, @PathVariable long id,
	// @ModelAttribute("cust") Customer cust,
	// Model model) {
	// if ("create".equals(flag)) {
	// Customer custNew = service.create(cust);
	// return custNew;
	// } else if ("update".equals(flag)) {
	// service.update(cust);
	// return cust;
	// } else if ("read".equals(flag)) {
	// Customer c = service.findById(id);
	// return c;
	// } else if ("delete".equals(flag)) {
	// service.delete(id);
	// return null;
	// }
	// return null;
	// }
	// <context-root>/customer/read/1
	@RequestMapping("/read/{id}")
	@ResponseBody
	public Customer read(@PathVariable long id) {
		return service.findById(id);
	}

	@RequestMapping("/create")
	@ResponseBody
	public Customer create(@ModelAttribute("cust") Customer cust) {
		return service.create(cust);
	}

	@RequestMapping("/update")
	@ResponseBody
	public int update(@ModelAttribute("cust") Customer cust) {
		return service.update(cust);
	}

	@RequestMapping("/delete/{id}")
	@ResponseBody
	public int delete(@PathVariable long id) {
		int ret = service.delete(id);
		List<Customer> custList = service.findAll();
		for (Customer customer : custList) {
			log.info(customer.toString());
		}

		return ret;
	}

	@RequestMapping("/search/{firstname}")
	@ResponseBody
	public Customer search(@PathVariable String firstname) {
		// System.out.println("Here is ");
		return service.findByName(firstname);
	}

	/**
	 * 启动的时候创建customer表，并插入10条数据
	 */
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		log.info("Creating tables");
		service.createTable();

		for (int i = 0; i < 10; i++) {
			Customer c = new Customer();
			c.setFirstName("Kobe" + i);
			c.setLastName("Bryant");
			c.setBirthDay(new Date(System.currentTimeMillis()));
			c.setWelthy(100000 * i);
			service.create(c);
			log.info("Creating customer: " + c.getId() + " -- " + c.getFirstName());
		}
	}
}
