package org.alive.springboot.sample.mapper;

import org.alive.springboot.sample.domain.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * mybatis mapper class. 只需要接口，加上注解即可；同时因为使用的内嵌H2数据库，所以数据源配置之类的都可以省略。
 * 
 * @author myumen
 * @since 2017.06.01
 */
@Mapper
public interface CustomerMapper {
	@Select("select * from customer where firstname = #{firstname}")
	public Customer findByName(String firstname);
}
