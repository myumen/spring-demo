package org.alive.springboot.sample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * index page
 * 
 * @author xuhailin730
 *
 */
@Controller
public class IndexController {

	@RequestMapping("/")
	public String index() {
		// return "index";
		return "index-layui";
	}
}
