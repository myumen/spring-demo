package org.alive.springboot.sample.domain;

import java.sql.Date;

public class Customer {
	private long id;
	private String firstName;
	private String lastName;
	private Date birthDay;
	private double welthy;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public double getWelthy() {
		return welthy;
	}

	public void setWelthy(double welthy) {
		this.welthy = welthy;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", birthDay=" + birthDay
				+ ", welthy=" + welthy + "]";
	}
}
