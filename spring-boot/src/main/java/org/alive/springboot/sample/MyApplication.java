package org.alive.springboot.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.WebApplicationInitializer;

/**
 * <p>
 * 应用程序启动类，继承SpringBootServletInitializer主要是为以了WAR形式部署在外部容器中，容器才能将应用识别为Web应用。Servlet3.0版本不需要web.xml文件。
 * 如果是Servlet
 * 2.5及以下的版本的容器，则需要通过在web.xml文件中增加加载Spring的代码，如配置Listener和DispatchServlet，具体可参考：
 * 
 * <a href=
 * "http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-create-a-deployable-war-file">howto-create-a-deployable-war-file</a>
 * 直接实现WebApplicationInitializer是为了支持部署到WebLogic；
 * </p>
 * 
 * @author XUHAILIN730
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableScheduling
public class MyApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

	public static void main(String[] args) {
		SpringApplication.run(MyApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MyApplication.class);
	}

}
