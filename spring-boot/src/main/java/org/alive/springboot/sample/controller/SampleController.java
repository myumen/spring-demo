package org.alive.springboot.sample.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.alive.springboot.sample.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("sample")
public class SampleController {

	@RequestMapping("")
	@ResponseBody
    public String index() {
        return "Index page";
    }
	
	@RequestMapping("/hello")
	@ResponseBody
    public String hello() {
        return "Hello World! - Powered by spring boot.";
    }
	
	@RequestMapping("/user/{name}")
    public String user(@PathVariable("name") String name, Model model) {
		model.addAttribute("name", name);
		// 寻找main/resources/templates/user.html模板文件
        return "user";
    }
	
	/**
	 * http://localhost:8080/get?name=ABC
	 * @param name
	 * @return
	 */
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> get(@RequestParam String name) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("title", "This is a map");
		map.put("name", name);
		return map;
	}
	
	/**
	 * http://localhost:8080/getuser/1000/ABC
	 * @param id
	 * @param name
	 * @return
	 */
	@RequestMapping("/getuser/{id}/{name}")
	@ResponseBody
	public User getUser(@PathVariable("id") int id, @PathVariable("name") String name) {
		User user = new User();
		user.setId(id);
		user.setName(name);
		user.setBirthDay(new Date());
		
		return user;
	}

}
