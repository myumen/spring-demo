package org.alive.springcloud.serviceconsumer.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 使用feign调用Service Provider
 * 
 * @author xuhailin730
 *
 */
@FeignClient(value = "SERVICE-PROVIDER")
public interface FeignService {
	@RequestMapping(value = "/service", method = RequestMethod.GET)
	String callServiceByFeign(@RequestParam(value = "name") String name);
}
