package org.alive.springcloud.serviceconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 服务消费者，通过服务注册找到服务提供者列表，调用Provider的方法，使用ribbon/Feign支持负载均衡
 * 
 * <P>
 * <ul>
 * <li>通过Ribbon访问： http://localhost:7070/service?name=Cusoumer</li>
 * <li>通过Feign访问： http://localhost:7070/service2?name=Kobe%20Bryant</li.
 * </ul>
 * </p>
 * @author xuhailin730
 *
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class ServiceConsumerApp {

	public static void main(String[] args) {
		SpringApplication.run(ServiceConsumerApp.class, args);
	}

	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
