package org.alive.springcloud.serviceconsumer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SampleService {
	
	@Autowired
	RestTemplate restTemplate;

	public String callService(String name) {
		return restTemplate.getForObject("http://SERVICE-PROVIDER/service?name=" + name, String.class);
	}
}
