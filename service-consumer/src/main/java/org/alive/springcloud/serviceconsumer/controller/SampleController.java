package org.alive.springcloud.serviceconsumer.controller;

import org.alive.springcloud.serviceconsumer.service.FeignService;
import org.alive.springcloud.serviceconsumer.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

	@Autowired
	SampleService service;

	@RequestMapping(value = "/service")
	public String hi(@RequestParam String name) {
		return service.callService(name);
	}

	@Autowired
	FeignService service2;

	@RequestMapping(value = "/service2", method = RequestMethod.GET)
	public String sayHi(@RequestParam String name) {
		return service2.callServiceByFeign(name);
	}

}
