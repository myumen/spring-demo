package org.alive.springcloud.serviceprovider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务提供者，通过修改server.port，启动多个实例，模拟集群的负载均衡效果
 * @author xuhailin730
 *
 */
@EnableEurekaClient
@RestController
public class ServiceController {
	@Value("${server.port}")
	private String port;

	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * 供消费者调用的服务方法 
	 *  
	 * @param name
	 * @return
	 */
	@RequestMapping("/service")
	public String service(@RequestParam String name) {
		System.out.println("Get called -------> " + name);
		return "hi " + name + ",i am from port:" + port;
	}
}
