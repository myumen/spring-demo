package org.alive.springcloud.serviceprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 服务提供者，通过Eureka客户端将应用注册到Eureka服务，供服务调用者查询并调用
 * 
 * @author xuhailin730
 *
 */
@EnableDiscoveryClient // 通过该注解，实现服务发现，注册
@SpringBootApplication
@EnableEurekaClient
public class ServiceProviderApp {

	public static void main(String[] args) {
		SpringApplication.run(ServiceProviderApp.class, args);

	}

}
